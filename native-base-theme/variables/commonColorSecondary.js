import color from "color";
import colors from '@lib/colors';
import primary from './commonColorPrimary';

const secondary = primary;

secondary.toolbarDefaultBg = colors.secondary.dark;
secondary.toolbarBtnTextColor = colors.secondary.light;
secondary.toolbarInputColor = colors.secondary.light;
secondary.toolbarBtnColor = colors.secondary.light;

export default secondary;