import color from "color";

export default {
  primary: {
    main: '#E73457',
    dark: color('#E73457').darken(0.03).hex(),
    light: color('#E73457').lighten(0.3).hex(),
    accent: '#F8B9C6',
    text: '#F8B9C6',
    textLight: '#FAFAFA'
  },
  secondary: {
    main: '#7B7776',
    dark: color('#7B7776').darken(0.03).hex(),
    light: color('#7B7776').lighten(0.8).hex(),
    lighten: color('#7B7776').lighten(1).hex(),
    accent: '#B1ADAC',
    text: '#D6D1CF',
    textLight: '#FAFAFA'
  }
};