import React from 'react';
import {
  View,
  StyleSheet
} from 'react-native';
import { Radio } from '@components';
import colors from '@lib/colors';

export default class RadioGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: props.items
    }
  }

  onChangeValue(key) {
    this.state.items.map((item, k) => {
      item.value = false;
      if (k == key) item.value = true;
    });
    this.setState({
      items: this.state.items
    });
  }

  render() {
    return (
      <View>
        {this.state.items.map((data, key) => (
          <Radio
            labelStyle={this.props.labelStyle}
            radioStyle={this.props.radioStyle}
            align={this.props.align}
            onChangeValue={this.onChangeValue.bind(this)}
            key={key}
            datakey={key}
            label={data.label}
            value={data.value} />
        ))}
      </View>
    );
  }
}