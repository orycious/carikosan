import React from 'react';
import DatePicker from 'react-native-datepicker';
import { Icon, StyleProvider } from 'native-base';
import { StyleSheet } from 'react-native';
import colors from '@lib/colors';
import moment from 'moment';
import "moment/locale/id";
moment.updateLocale('id');

export default class CostumDatePicker extends React.Component {
  constructor(props){
    super(props)
    this.state = {date: null}
  }

  render() {
    let min = new Date;
    let max = new Date;
    max.setFullYear(max.getFullYear() + 1);
    return(
      <DatePicker
        style={[{width: null}, this.props.style]}
        date={this.state.date}
        mode="date"
        placeholder={(this.props.placeholder)? this.props.placeholder: moment().format("DD MMMM YYYY")}
        format="DD MMMM YYYY"
        minDate={min}
        maxDate={max}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        iconComponent={<Icon name='ios-calendar-outline' style={styles.icon}/>}
        customStyles={{
          dateInput: {
            alignItems: 'flex-start',
            padding: 10,
            borderColor: colors.secondary.accent,
            borderWidth: 0.5,
            height: 50
          },
          dateTouchBody: {
            height: 50
          },
          dateText: {
            fontSize: 15,
            color: '#000'
          },
          placeholderText: {
            fontSize: 15,
            color: colors.secondary.accent
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {this.setState({date: date})}}
      />
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    color: colors.secondary.accent,
    paddingHorizontal: 12,
    fontSize: 25,
    position: 'absolute',
    right: 0
  }
})