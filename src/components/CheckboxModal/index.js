import React from 'react';
import {
  View,
  StyleSheet
} from 'react-native';
import {
  Text,
  Button,
  Icon,
  Right,
  Left,
  getTheme,
  StyleProvider
} from 'native-base';
import { Checkbox } from '@components';
import colors from '@lib/colors';
import DialogManager, { ScaleAnimation, DialogContent } from 'react-native-dialog-component';
import commonColor from '@native-base-theme/variables/commonColorPrimary';

export default class CheckboxModal extends React.Component {
  constructor(props) {
    super(props);
  }

  submit() {
    DialogManager.dismissAll(() => {
      if (this.props.onSubmit) this.props.onSubmit();
    });
  }

  openDialog() {
    DialogManager.show({
      title: this.props.title,
      titleAlign: 'flex-start',
      titleTextStyle: [styles.color, styles.italic],
      animationDuration: 200,
      ScaleAnimation: new ScaleAnimation(),
      width: null,
      dialogStyle: styles.dialogStyle,
      children: (
        <DialogContent contentStyle={styles.contentDialogStyle}>
          <StyleProvider style={getTheme(commonColor)}>
            <View>
              {this.props.items.map((data, key) => <Checkbox key={key} label={data.label} value={data.value} />)}
              <Button primary bordered block rounded small
                style={styles.buttonDialog}
                onPress={this.submit.bind(this)}
              >
                <Text>SELESAI</Text>
              </Button>
            </View>
          </StyleProvider>
        </DialogContent>
      ),
    }, () => {
      if (this.props.onOpen) this.props.onOpen();
    });
  }

  render() {
    return (
      <Button bordered dark block iconRight
        style={styles.button}
        onPress={this.openDialog.bind(this)}>
        <View style={styles.insideButton}>
          <Left>
            <Text style={[styles.color, styles.italic]}>{this.props.title}</Text>
          </Left>
          <Right>
            <Icon name="ios-arrow-down" style={styles.color} />
          </Right>
        </View>
      </Button>
    );
  }
}

const styles = StyleSheet.create({
  buttonDialog: {
    marginHorizontal: 30,
    marginTop: 10
  },
  contentDialogStyle: {
    paddingHorizontal: 20
  },
  dialogStyle: {
    flex: 1,
    position: 'absolute',
    left: 20,
    right: 20,
    paddingHorizontal: 0
  },
  italic: { fontStyle: 'italic' },
  color: {
    color: colors.secondary.accent
  },
  button: {
    borderColor: colors.secondary.light,
    borderRadius: 0,
    marginVertical: 10
  },
  insideButton: {
    flexDirection: 'row',
    paddingHorizontal: 10
  },
})