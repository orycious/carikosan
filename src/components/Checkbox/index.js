import React from 'react';
import {
  CheckBox,
  Right,
  Text
} from 'native-base';
import {
  View,
  StyleSheet
} from 'react-native';
import colors from '@lib/colors';

export default class CostumCheckbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value
    }
  }

  changeValue() {
    if (this.state.value) this.setState({value: false});
    else this.setState({value: true});
    if (this.props.onChangeValue) this.props.onChangeValue();
  }

  getValue() {
    return this.state.value;
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <Text>{this.props.label}</Text>
        <Right>
          <CheckBox
            checked={this.state.value}
            onPress = {this.changeValue.bind(this)}/>
        </Right>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: 'center',
    flexDirection: 'row',
    paddingVertical: 10,
    paddingRight: 10
  },
})