import React from 'react';
import {
  Radio,
  Right,
  Left,
  Text
} from 'native-base';
import {
  View,
  StyleSheet
} from 'react-native';
import colors from '@lib/colors';

export default class CostumRadio extends React.Component {
  changeValue() {
    if (this.props.onChangeValue) this.props.onChangeValue(this.props.datakey);
  }

  getValue() {
    return this.props.value;
  }

  render() {
    return (
      <View style={[styles.wrapper, this.props.radioStyle]}>
        {(!this.props.align || this.props.align == 'right')?<Text style={this.props.labelStyle}>{this.props.label}</Text>:null}
        {(!this.props.align || this.props.align == 'right')?
        (<Right>
          <Radio
            color={colors.secondary.accent}
            selected={this.props.value}
            onPress = {this.changeValue.bind(this)}/>
        </Right>):
        (<Radio
            color={colors.secondary.accent}
            selected={this.props.value}
            onPress = {this.changeValue.bind(this)}/>)}
        {(this.props.align == 'left')?<Text style={this.props.labelStyle}> {this.props.label}</Text>:null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingVertical: 10,
    paddingRight: 10
  }
})