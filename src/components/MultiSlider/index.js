import React from 'react';
import { StyleSheet } from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import colors from '@lib/colors';

export default class CustomMultiSlider extends React.Component {
  render() {
    return (
      <MultiSlider
        {...this.props}
        containerStyle={styles.container}
        selectedStyle={styles.colorMain}
        markerStyle={styles.marker}
        trackStyle={styles.track} />
    );
  }
}

const styles = StyleSheet.create({
  track: {
    height: 4,
    borderRadius: 3
  },
  marker: {
    backgroundColor: '#fff',
    borderWidth: 0.9,
    borderColor: 'silver',
    width: 20,
    height: 20
  },
  colorMain: {
    backgroundColor: colors.primary.main,
  },
  container: {
    paddingHorizontal: 10,
    margin: 0,
    paddingVertical: 0,
    overflow: 'hidden',
    alignItems: 'center',
    height: 0
  },
})