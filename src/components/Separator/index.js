import React from 'react';
import {
  View,
  StyleSheet
} from 'react-native';
import colors from '@lib/colors';

export default class Separator extends React.Component {
  render() {
    return (
      <View style={[styles.separator, this.props.style]}>
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  separator: {
    borderColor: colors.secondary.light,
    borderWidth: 1,
    marginVertical: 20
  },
})