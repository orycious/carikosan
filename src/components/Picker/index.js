import React from 'react';
import {
  View,
  Form,
  Picker,
  Button,
  Icon
} from 'native-base';
import { StyleSheet } from 'react-native';
import colors from '@lib/colors';

export default class CostumPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: undefined,
      items: ('items' in props) ? props.items : []
    }
  }

  onValueChange(value) {
    this.setState({ value });
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <Icon
            name='ios-arrow-forward'
            style={styles.icon} />
        <Picker
          mode="dialog"
          placeholder="Select One"
          selectedValue={this.state.value}
          onValueChange={this.onValueChange.bind(this)}
          style={styles.picker}
          {...this.props}
        >
          
          {this.state.items.map((item, key) => <Picker.Item key={key} label={item.label} value={item.value} />)}

        </Picker>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    color: colors.secondary.accent,
    paddingHorizontal: 12,
    position: 'absolute',
    alignSelf: 'flex-end',
    fontSize: 25
  },
  picker: {
    backgroundColor: '#ffffff00',
    margin: 0,
    padding: 0
  },
  wrapper: {
    justifyContent: 'center',
    borderColor: colors.secondary.accent,
    borderWidth: 0.5,
    padding: 0
  },
})