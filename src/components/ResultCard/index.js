import React from 'react';
import { StyleSheet, Image } from 'react-native';
import {
  Card,
  CardItem,
  View,
  Left,
  Right,
  Body,
  Text
} from 'native-base';
import StarRating from 'react-native-star-rating';
import colors from '@lib/colors';

export default class ResultCard extends React.Component {
  render() {
    return (
      <Card transparent style={styles.card}>
        <CardItem cardBody>
          <Image source={{ uri: 'http://jktpress.com/wp-content/uploads/2017/09/hewan-apa-yang-pertama-kamu-lihat-di-4-gambar-ini.jpg' }} style={{ height: 200, width: null, flex: 1 }} />
        </CardItem>
        <CardItem cardBody>
          <Body style={styles.cardBody}>
            <View style={styles.row}>
              <Left>
                <Text style={styles.harga}>Rp 1000.000/bulan</Text>
              </Left>
              <Right>
              <StarRating
                disabled={true}
                emptyStar={'ios-star-outline'}
                fullStar={'ios-star'}
                halfStar={'ios-star-half'}
                iconSet={'Ionicons'}
                maxStars={5}
                starSize={20}
                rating={5}
                starColor={'red'}
              />
              </Right>
            </View>
            <View style={styles.row}>
              <Text style={styles.namaKost}>Kost Rumah Asri </Text>
              <Text note style={styles.alamat}> |  jl Lely Geneng Timur</Text>
            </View>
          </Body>
        </CardItem>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  alamat: {
    fontSize: 15
  },
  namaKost: {
    fontWeight: 'bold',
    fontSize: 15
  },
  harga: {
    fontSize: 16
  },
  card: {
    borderColor: 'transparent',
    marginBottom: 20
  },
  row: {
    flexDirection: 'row'
  },
  cardBody: {
    paddingVertical: 7
  },
})