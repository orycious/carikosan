const createReducer = (initialState, handlers) => (state = initialState, action) => {
  if ('type' in handlers) return handlers[action.type](state, action);
  else return state;
}

export {
  createReducer
};