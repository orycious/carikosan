import React from 'react';
import {
  Text,
  View
} from 'native-base';
import { StyleSheet } from 'react-native';
import { Picker } from '@components';

export default class PickerSearch extends React.Component {
  render() {
    return (
      <View style={{
        marginBottom: 12
      }}>
        <Text style={styles.title}>{this.props.title}</Text>
        <Picker placeholder={this.props.placeholder} items={this.props.items} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    marginBottom: 3
  },
})