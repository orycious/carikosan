import React from 'react';
import {
  Container,
  Content,
  Text,
  H3,
  View,
  Form,
  Button,
  Icon
} from 'native-base';
import {
  ImageBackground,
  StyleSheet
} from 'react-native';
import colors from '@lib/colors';

import Picker from './Picker';
import DatePicker from './DatePicker';
import TextField from './TextField';

export default class Search extends React.Component {
  render() {
    return (
      <Container>

        <Content>

          <View style={styles.bannerWrapper}>
            <ImageBackground source={require('@images/bg.jpg')} style={styles.backgroundBannerWrapper}>
              <Text style={[styles.heading, styles.bannerGreeting]}>Hello!</Text>
              <H3 style={styles.heading}>Let's find you a room</H3>
            </ImageBackground>
          </View>

          <View style={styles.content}>

            <Form>
              <Picker
                title="Kota / City"
                placeholder="Jakarta / Bandung / ..."
                items={[
                  { label: 'Jakarta', value: 'jakarta' },
                  { label: 'Bandung', value: 'bandung' },
                  { label: 'Surabaya', value: 'surabaya' }
                ]} />

              <TextField
                title="Dekat dengan / Near With"
                placeholder="Kampus ITB / Kampus UGM / ..." />

              <Picker
                title="Waktu / Time type"
                placeholder="Harian / Bulanan / Tahunan"
                items={[
                  { label: 'Jakarta', value: 'jakarta' },
                  { label: 'Bandung', value: 'bandung' },
                  { label: 'Surabaya', value: 'surabaya' }
                ]} />

              <DatePicker
                title="Masuk / Check in"
                placeholder="Harian / Bulanan / Tahunan" />

              <DatePicker
                title="Keluar / Check out"
                placeholder="Harian / Bulanan / Tahunan" />

              <Picker
                title="Lama tinggal / How long"
                placeholder="Lama sewa (angka)"
                items={[
                  { label: 'Jakarta', value: 'jakarta' },
                  { label: 'Bandung', value: 'bandung' },
                  { label: 'Surabaya', value: 'surabaya' }
                ]} />

              <View style={styles.buttonWrapper}>
                <Button iconRight rounded dark>
                  <Text>ADVANCE SEARCH</Text>
                  <Icon name='options' />
                </Button>

                <Button rounded primary>
                  <Text>SEARCH NOW</Text>
                </Button>
              </View>

            </Form>

          </View>

        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  buttonWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  content: {
    padding: 20
  },
  bannerGreeting: {
    fontSize: 40
  },
  backgroundBannerWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  heading: {
    color: colors.secondary.textLight,
    fontFamily: 'Roboto'
  },
  bannerWrapper: {
    height: 170
  },
})