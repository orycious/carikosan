import React from 'react';
import {
  Text,
  View
} from 'native-base';
import { StyleSheet } from 'react-native';
import { DatePicker } from '@components';

export default class DatePickerSearch extends React.Component {
  render() {
    return (
      <View style={{
        marginBottom: 12
      }}>
        <Text style={styles.title}>{this.props.title}</Text>
        <DatePicker placeholder={this.props.placeholder} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    marginBottom: 3
  },
})