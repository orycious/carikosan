import React from 'react';
import {
  Text,
  Item,
  Input,
  Icon,
  View
} from 'native-base';
import { StyleSheet } from 'react-native';
import colors from '@lib/colors';

export default class PickerSearch extends React.Component {
  render() {
    return (
      <View style={{
        marginBottom: 12
      }}>
        <Text style={styles.title}>{this.props.title}</Text>
        <Item regular>
          <Input placeholder={this.props.placeholder}/>
          <Icon style={styles.icon} name='ios-arrow-forward' />
        </Item>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    marginBottom: 3
  },
  icon: {
    color: colors.secondary.accent
  },
})