import React from 'react';
import { StyleSheet } from 'react-native';
import {
  View,
  Text
} from 'native-base';
import {
  DatePicker
} from '@components';
import Kamar from './Kamar';
import colors from '@lib/colors';

export default class PilihKamar extends React.Component {
  render() {
    return (
      <View>
        <View style={[styles.wrapper, { backgroundColor: colors.secondary.lighten }]}>
          <Text>Masuk / Check in</Text>
          <DatePicker style={[{ marginBottom: 12 }, styles.inputDate]} />
          <Text>Keluar / Check out</Text>
          <DatePicker style={styles.inputDate} />
        </View>
        <View style={styles.wrapper}>

        <Kamar />
        <Kamar />

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputDate: {
    backgroundColor: '#FFF'
  },
  row: {
    flexDirection: 'row',
  },
  wrapper: {
    padding: 20
  },
})