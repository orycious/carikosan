import React from 'react';
import { StyleSheet } from 'react-native';
import {
  Card,
  CardItem,
  Left,
  Body,
  Right,
  Text,
  View,
  Thumbnail
} from 'native-base';
import StarRating from 'react-native-star-rating';
import {
  Separator
} from '@components';
import colors from '@lib/colors';

export default class ReviewItem extends React.Component {
  render() {
    return (
      <Card transparent style={styles.card}>
        <CardItem>
          <Left>
            <Thumbnail small source={{ uri: 'http://img.timeinc.net/time/daily/2010/1011/poy_nomination_agassi.jpg' }} />
            <Body>
              <Text style={styles.nama}>Me</Text>
              <Text note style={[styles.colorAccent, styles.alamat]}>Januari 2018</Text>
            </Body>
            <Right>
              <StarRating
                disabled={true}
                emptyStar={'ios-star-outline'}
                fullStar={'ios-star'}
                halfStar={'ios-star-half'}
                iconSet={'Ionicons'}
                maxStars={5}
                starSize={15}
                rating={5}
                starColor={'red'}
              />
            </Right>
          </Left>
        </CardItem>
        <CardItem>
          <Text style={[styles.colorAccent, styles.contentText]}>the first Web-based view and redline tool that not only lets users View, Annotate and Print over 200 document formats but also allows multiple attendees or reviewers to view/markup the document at the same time!</Text>
        </CardItem>
        <CardItem>
          <Body>
            <Text style={[styles.nama, styles.colorMain]}>Response from Host</Text>
            <Text note style={[styles.colorMain, styles.alamat, {marginBottom: 5}]}>Januari 2018</Text>
            <Text note style={[styles.colorMain, styles.contentText]}>ThankYou for your Review I hope You always comfort and coming back for more</Text>
          </Body>
        </CardItem>
        <View style={styles.separatorWrapper}>
          <Separator style={styles.separator} />
        </View>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  colorMain: { color: colors.primary.light },
  contentText: {
    textAlign: 'justify'
  },
  alamat: {
    fontSize: 14
  },
  nama: {
    fontWeight: 'bold'
  },
  colorAccent: { color: colors.secondary.accent },
  separator: { marginTop: 10, marginBottom: 0 },
  separatorWrapper: { paddingHorizontal: 20 },
  card: {
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0
  },
})