import React from 'react';
import { StyleSheet, TouchableHighlight } from 'react-native';
import {
  Container,
  Header,
  Button,
  Segment,
  Text,
  Icon,
  Body,
  Content,
  getTheme,
  StyleProvider
} from 'native-base';
import theme from '@native-base-theme/variables/commonColorSecondary';
import Overview from './Overview';
import PilihKamar from './PilihKamar';
import Review from './Review';
import colors from '@lib/colors';

export default class DetailAkomodasi extends React.Component {
  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
        <Container>
          <Header>
            <Body style={styles.bodyHeader}>
              <Button transparent style={{ alignSelf: 'flex-start', marginLeft: -10 }}>
                <Icon style={styles.headerIcon} name="ios-arrow-back" />
              </Button>
              <Segment style={styles.segment}>
                <TouchableHighlight style={styles.btnSegment}>
                  <Text style={styles.textBtnSegment} uppercase={false}>Overview</Text>
                </TouchableHighlight>
                <TouchableHighlight style={styles.btnSegment}>
                  <Text style={styles.textBtnSegment} uppercase={false}>Pilih Kamar</Text>
                </TouchableHighlight>
                <TouchableHighlight style={styles.btnSegment}>
                  <Text style={styles.textBtnSegment} uppercase={false}>Review</Text>
                </TouchableHighlight>
              </Segment>
            </Body>
          </Header>

          <Content style={{ backgroundColor: '#fff' }}>

            <Review />
            
          </Content>
        </Container>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  textBtnSegment: { fontSize: 14, color: colors.secondary.light },
  btnSegment: { padding: 10, paddingHorizontal: 20 },
  segment: { alignSelf: 'flex-start', alignItems: 'center', justifyContent: "flex-start"},
  bodyHeader: { flex: 1, flexDirection: 'row' },
  headerIcon: {
    fontSize: 30,
    color: theme.toolbarBtnColor
  }
})