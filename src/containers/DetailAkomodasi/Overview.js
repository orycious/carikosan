import React from 'react';
import { StyleSheet, Image } from 'react-native';
import {
  View,
  H3,
  Item,
  Text,
  Icon
} from 'native-base';
import {
  Separator
} from '@components';
import colors from '@lib/colors';

export default class Overview extends React.Component {
  render() {
    return (
      <View>
        <Image source={{ uri: 'https://1.bp.blogspot.com/-PfcCWR8naN0/WCCYanenu5I/AAAAAAAAJRA/VuwCGAxYidA3l35v5gSspxQSPNt36sGpQCLcB/s1600/gbr--model-rumah-kos-kosan-minimalis-3.jpg' }} style={{ height: 200, width: null, flex: 1 }} />
        <View style={styles.wrapper}>

          <View style={styles.row}>
            <View style={{ flex: 1 }}>
              <H3 style={[styles.title, { marginBottom: 7 }]}>KOST RUMAH ASRI</H3>
              <Text note style={{ marginBottom: 2 }}>Jl. Lely Geneng Timur</Text>
              <View style={styles.row}>
                <Icon style={styles.icon} name="logo-whatsapp" />
                <Icon style={styles.icon} name="ios-call-outline" />
                <Text note>+6281252267634</Text>
              </View>
            </View>
            <View>
              <Item regular style={styles.mapBox}>
              </Item>
            </View>
          </View>

          <Separator />

          <Text style={styles.subtitle}>Tentang hunian:</Text>
          <Text note>MANUSIA PRA ASKSARAMasyarakat pra aksara adalah gambaran tentang kehidupan manusia – manusia pada masa lampau, di mana mereka belum mengenal tulisan atau istilah lain. Untuk menamakan zaman pra aksara yaitu zaman Nirleka. Nir artinya tidak ada dan leka artinya tulisan</Text>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  subtitle: {
    fontWeight: 'bold',
    color: colors.secondary.accent
  },
  icon: {
    fontSize: 20,
    marginRight: 5,
    color: colors.secondary.accent
  },
  mapBox: {
    width: 80,
    height: 80
  },
  title: {
    fontSize: 18
  },
  row: {
    flexDirection: 'row',
  },
  wrapper: {
    padding: 20
  },
})