import React from 'react';
import { StyleSheet, Image } from 'react-native';
import {
  View,
  Text,
  Icon,
  Button
} from 'native-base';
import {
  Separator,
  RadioGroup
} from '@components';
import colors from '@lib/colors';

export default class Kamar extends React.Component {
  render() {
    return (
      <View>
        <Image source={{ uri: 'https://1.bp.blogspot.com/-PfcCWR8naN0/WCCYanenu5I/AAAAAAAAJRA/VuwCGAxYidA3l35v5gSspxQSPNt36sGpQCLcB/s1600/gbr--model-rumah-kos-kosan-minimalis-3.jpg' }} style={{ height: 150, width: null, flex: 1, marginBottom: 10 }} />
        <View style={[styles.row, { justifyContent: 'space-between', marginBottom: 10 }]}>
          <Icon style={styles.icon} name="ios-wifi-outline" />
          <Icon style={styles.icon} name="ios-car-outline" />
        </View>

        <View style={styles.row} >
          <View style={{ flex: 1 }}>
            <Text style={styles.subtitle}>Kamar Premium</Text>
            <Text style={styles.desc}>Letak: Lantai Dasar</Text>
            <Text style={styles.desc}>Luas: 3 X 4 m</Text>
          </View>
          <View style={{ flex: 1, marginBottom: 10 }}>
            <Text style={styles.subtitle}>Pesan kamar</Text>
            <RadioGroup
              align="left"
              radioStyle={styles.radioStyle}
              labelStyle={styles.labelRadioStyle}
              items={[
                { label: "Rp. 10.000 / hari", value: false },
                { label: "Rp. 20.000 / bulan", value: false },
                { label: "Rp. 30.000 / tahun", value: false }
              ]}
            />
          </View>
        </View>

        <Button primary block rounded small>
          <Text>Pesan Kamar</Text>
        </Button>

        <Separator />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  labelRadioStyle: {
    color: colors.secondary.accent
  },
  radioStyle: {
    paddingVertical: 3
  },
  inputDate: {
    backgroundColor: '#FFF'
  },
  subtitle: {
    fontWeight: 'bold',
    color: colors.secondary.accent
  },
  icon: {
    fontSize: 25,
    marginRight: 5,
    color: colors.secondary.accent
  },
  desc: {
    color: colors.secondary.accent,
    marginBottom: 5
  },
  row: {
    flexDirection: 'row',
  }
})