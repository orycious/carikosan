import React from 'react';
import { StyleSheet } from 'react-native';
import {
  Container,
  Header,
  Button,
  Icon,
  Body,
  Content,
  Item,
  Input,
  View,
  getTheme,
  StyleProvider
} from 'native-base';
import { ResultCard } from '@components';
import theme from '@native-base-theme/variables/commonColorSecondary';
import colors from '@lib/colors';

export default class ResultSearch extends React.Component {
  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
        <Container>
          <Header style={styles.header}>
            <Body style={styles.bodyHeader}>
              <Button transparent>
                <Icon style={styles.headerIcon} name='ios-arrow-back' />
              </Button>
              <Item style={styles.inputBox}>
                <Icon style={styles.iconFormHeader} name='ios-search' />
                <Input style={styles.input} placeholderTextColor={theme.toolbarInputColor} placeholder='Icon Textbox' />
              </Item>
            </Body>
          </Header>

          <Content style={{backgroundColor: '#fff'}}>
            <View style={styles.wrapper}>

              <ResultCard />
              <ResultCard />

            </View>
          </Content>
        </Container>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    padding: 20
  },
  input: {
    color: theme.toolbarInputColor,
    padding: 0
  },
  inputBox: {
    backgroundColor: colors.secondary.accent,
    paddingHorizontal: 10,
    paddingVertical: 0,
    borderWidth: 0
  },
  header: {
    height: null
  },
  bodyHeader: {
    paddingVertical: 10,
    flexDirection: 'row',
    marginLeft: -10,
    backgroundColor: 'transparent'
  },
  iconFormHeader: {
    color: theme.toolbarBtnColor
  },
  headerIcon: {
    fontSize: 30,
    paddingHorizontal: 3,
    color: theme.toolbarBtnColor
  }
})