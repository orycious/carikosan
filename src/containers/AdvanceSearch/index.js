import React from 'react';
import {
  Container,
  Header,
  Left,
  Right,
  Button,
  Icon,
  Body,
  Title,
  Text,
  Content,
  StyleProvider,
  getTheme
} from 'native-base';
import { StyleSheet, View } from 'react-native';
import colors from '@lib/colors';
import theme from '@native-base-theme/variables/commonColorSecondary';
import MultiSlider from './MultiSlider';
import {
  Separator,
  CheckboxModal,
  Checkbox,
  RadioGroup
} from '@components';

theme.toolbarDefaultBg = colors.secondary.light;
theme.toolbarBtnTextColor = colors.secondary.dark;
theme.toolbarInputColor = colors.secondary.dark;
theme.toolbarBtnColor = colors.secondary.dark;

export default class AdvanceSearch extends React.Component {
  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
        <Container>
          <Header style={styles.header}>
            <Left>
              <Button transparent dark>
                <Icon name='ios-close' />
              </Button>
            </Left>
            <Body>
              <Title style={styles.textHeader}>ADVANCE SEARCH</Title>
            </Body>
            <Right>
              <Button transparent dark>
                <Title style={styles.textHeader}>Reset</Title>
              </Button>
            </Right>
          </Header>

          <Content>
          <View style={styles.content}>

            <Text style={styles.title}>Range harga</Text>
            <MultiSlider
              values={[100000, 2570000]}
              step={1000}
              min={100000}
              max={5000000}
              sliderLength={300}
            />

            <Separator />

            <Text style={styles.title}>Tipe Akomodasi</Text>
            <CheckboxModal
              title="Pilih tipe"
              items={[
                {label: 'indeKost', value: true},
                {label: 'Villa', value: false},
                {label: 'Asrama', value: false}
              ]}
            />

            <Separator />
              
            <Text style={styles.title}>Tipe Kamar</Text>
            <RadioGroup
              items={[
                {label: "Pria / Male", value: false},
                {label: "Wanita / Female", value: false},
                {label: "Pria & Wanita / Male & Female", value: false}
              ]}
            />

            <Separator />

            <Text style={styles.title}>Fasilitas</Text>
            <Checkbox label="Air Conditioner (AC)" value={false} />
            <Checkbox label="Air Panas / Hot Water" value={false} />


          </View>
          </Content>
        </Container>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 10
  },
  content: {
    padding: 20
  },
  header: {
    height: 45
  },
  textHeader: {
    color: colors.secondary.dark,
    fontSize: 15,
    fontFamily: 'Roboto'
  },
})