import React from 'react';
import { MultiSlider } from '@components';
import { StyleSheet, View } from 'react-native';
import {
  Text,
  Left,
  Right
} from 'native-base';
import accounting from '@lib/accounting';

export default class MultiSliderAdvanceSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      values: props.values
    }
  }

  updateCaption(values) {
    this.setState({
      values
    });
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.wrapperCaption}>
          <Left>
            <Text style={styles.caption}>{accounting.rupiah(this.state.values[0])}</Text>
          </Left>
          <Right>
            <Text style={styles.caption}>{accounting.rupiah(this.state.values[1])}</Text>
          </Right>
        </View>
        <MultiSlider
          onValuesChange={this.updateCaption.bind(this)}
          {...this.props} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  caption: {
    fontSize: 15
  },
  wrapperCaption: {
    marginBottom: 20,
    flexDirection: 'row'
  },
  wrapper: {
    paddingBottom: 10
  },
})