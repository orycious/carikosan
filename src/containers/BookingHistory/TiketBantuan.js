import React, { Component } from 'react';
import {
    Container,
    Header,
    Footer,
    Button,
    Left,
    Item,
    Title,
    Body,
    Form,
    Input,
    List,
    Content,
    Right,
    Text,
    View,
    ListItem,
    Radio
} from 'native-base';
import { styles } from './BookingHistory';

class Keluhan extends Component {
    render() {
        return (
            <Input
                {...this.props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
                editable={true}
            />
        );
    }
}

export default class TiketBantuan extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        const { garis2, flex1, namakos, font, check, date, date2, price, fasilitas } = styles;
        return (
            <Container>
                <Header>
                    <Left style={flex1} />
                    <Body style={styles.bodyHeader}>
                        <Title style={font}>TIKET BANTUAN</Title>
                    </Body>
                    <Right style={flex1} />
                </Header>
                <Content>
                    <View style={{ padding: 15 }}>
                        <Text style={{ fontWeight: '500' }}>Tipe Keluhan</Text>
                    </View>
                    <List>
                        <ListItem>
                            <Radio selected={true} />
                            <Text style={{ marginLeft: 15 }}>Pembayaran</Text>
                        </ListItem>
                        <ListItem>
                            <Radio selected={false} />
                            <Text style={{ marginLeft: 15 }}>Aplikasi</Text>
                        </ListItem>
                        <ListItem>
                            <Radio selected={false} />
                            <Text style={{ marginLeft: 15 }}>Pemesanan</Text>
                        </ListItem>
                    </List>
                    <View style={{ padding: 15 }}>
                        <Text style={{ fontWeight: '500' }}>Subjek</Text>
                    </View>
                    <Form>
                        <View style={{ borderWidth: 1, borderColor: '#dfe6e9', marginHorizontal: 15 }}>
                            <Input style={{ fontSize: 16, marginLeft: 10 }} placeholder="Subjek keluhan / pertanyaan" />
                        </View>
                    </Form>
                    <View style={{ padding: 15 }}>
                        <Text style={{ fontWeight: '500' }}>Keluhan</Text>
                    </View>
                    <Form>
                        <View style={{ borderWidth: 1, borderColor: '#dfe6e9', marginHorizontal: 15, marginBottom: 15, flexDirection: 'row', alignItems: 'flex-start' }}>
                            <Keluhan style={{ fontSize: 16, marginLeft: 10 }}
                                placeholder="Masukan keluhan / pertanyaan"
                                multiline={true}
                                numberOfLines={6}
                                onChangeText={(text) => this.setState({ text })}
                                value={this.state.text}
                            />
                        </View>
                    </Form>
                </Content>
                <Footer style={{ backgroundColor: 'white', alignItems: 'center' }}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around', paddingHorizontal: 20 }}>
                        <Button bordered rounded style={{ width: '100%', height: 40, justifyContent: 'center', borderColor: 'rgb(240, 64, 87)' }}>
                            <Text style={{ color: '#f04057' }}>LAPORKAN</Text>
                        </Button>
                    </View>
                </Footer>
            </Container>
        );
    }
}
