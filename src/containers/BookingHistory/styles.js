import { StyleSheet } from 'react-native';
import theme from '@native-base-theme/variables/commonColorSecondary';

const styles = StyleSheet.create({
    bodyHeader: { 
        flex: 3, 
        justifyContent: 'center', 
        alignItems: 'center' 
    },
    headerIcon: {
        fontSize: 25,
        color: theme.toolbarBtnColor
    },
    font: {
        fontWeight: '500'
    },
    tombol: {
        marginLeft: -1
    },
    flex1: {
        flex: 1
    },
    flex2: {
        flex: 6
    },
    flex3: {
        flex: 6
    },
    badge: {
        width: 70,
        height: 25,
        marginTop: -43,
        marginLeft: 260,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    garis: {
        borderWidth: 0.5,
        borderColor: '#dbdbdb',
        marginTop: 25,
        marginBottom: 10,
        width: '100%'
    },
    garis2: {
        borderWidth: 0.5,
        borderColor: '#dbdbdb',
        marginTop: 1,
        marginBottom: 7,
        width: '100%'
    },
    garis3: {
        borderWidth: 0.5,
        borderColor: '#dbdbdb',
        marginTop: 1,
        marginBottom: 1,
        width: '100%'
    },
    namakos: {
        fontSize: 26,
        fontWeight: '200',
        color: 'black'
    },
    check: { 
        flexDirection: 'row', 
        justifyContent: 'space-around' 
    },
    fasilitas: { 
        marginBottom: 6, 
        marginLeft: 160 
    },
    price: { 
        fontSize: 16, 
        fontWeight: '400', 
        marginBottom: 10 
    },
    date: { 
        marginLeft: 25, 
        color: '#F04057' 
    },
    date2: {
        marginRight: 25,
        color: '#F04057'
    },
    date3: {
        marginTop: 2,
        marginRight: -10,
        fontSize: 12
    },
    email: {
        color: '#F04057',
        fontSize: 24

    },
    temail: {
        marginTop: -40,
    },
    fonotif: {
        fontWeight: '500',
        marginLeft: 15,
        marginTop: 2
    },
    keluhan: {
        
    }
})

export { styles };