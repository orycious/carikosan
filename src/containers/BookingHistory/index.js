import React from 'react';
import {
    Container,
    Button,
    Left,
    Header,
    Title,
    Body,
    Icon,
    Content,
    Right,
    Text,
    View,
} from 'native-base';
import { styles } from './BookingHistory';

export default class ReviewItem extends React.Component {
    render() {
        const { bodyHeader, headerIcon, font, flex1, tombol, garis } = styles;
        return (
            <Container>
                <Header>
                    <Left style={flex1} />
                    <Body style={bodyHeader}>
                        <Title style={font}>Riwayat Pemesanan</Title>
                    </Body>
                    <Right style={flex1}>
                        <Button transparent style={tombol}>
                            <Icon style={headerIcon} name="ios-refresh" />
                        </Button>
                    </Right>
                </Header>
                <Content style={{ backgroundColor: '#fff' }}>
                    <View style={{ padding: 13 }}>
                        <Text style={font}>
                            CK1802000422
                    </Text>
                        <Text note>
                            Kos Cipendawa [Test]
                    </Text>
                        <Text note>
                            Kamar No. 203
                    </Text>
                        <View style={{ width: 70, height: 25, backgroundColor: 'orange', marginTop: -43, marginLeft: 260, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 10, color: 'white' }}>
                                EXPIRED
                    </Text>
                        </View>
                    </View>
                    <View
                        style={garis}
                    />
                    <View style={{ padding: 13 }}>
                        <Text style={font}>
                        CK1802000421
                    </Text>
                        <Text note>
                            Kos Cipendawa [Test]
                    </Text>
                        <Text note>
                            Kamar No. 203
                    </Text>
                        <View style={{ width: 70, height: 25, backgroundColor: 'red', marginTop: -43, marginLeft: 260, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 10, color: 'white' }}>
                                CANCELED
                    </Text>
                        </View>
                    </View>
                    <View
                        style={garis}
                    />
                    <View style={{ padding: 13 }}>
                        <Text style={font}>
                            CK1802000420
                    </Text>
                        <Text note>
                            Kos Cipendawa [Test]
                    </Text>
                        <Text note>
                            Kamar No. 203
                    </Text>
                        <View style={{ width: 70, height: 25, backgroundColor: '#81caf4', marginTop: -43, marginLeft: 260, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 10, color: 'white' }}>
                                CHECKIN
                    </Text>
                        </View>
                    </View>
                    <View
                        style={garis}
                    />
                    <View style={{ padding: 13 }}>
                        <Text style={font}>
                            CK1802000419
                    </Text>
                        <Text note>
                            Kos Cipendawa [Test]
                    </Text>
                        <Text note>
                            Kamar No. 203
                    </Text>
                        <View style={{ width: 70, height: 25, backgroundColor: 'grey', marginTop: -43, marginLeft: 260, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 10, color: 'white' }}>
                                PENDING
                    </Text>
                        </View>
                    </View>
                    <View
                        style={garis}
                    />
                    <View style={{ padding: 13 }}>
                        <Text style={font}>
                            CK1802000418
                    </Text>
                        <Text note>
                            Kos Cipendawa [Test]
                    </Text>
                        <Text note>
                            Kamar No. 203
                    </Text>
                        <View style={{ width: 70, height: 25, backgroundColor: 'green', marginTop: -43, marginLeft: 260, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 10, color: 'white' }}>
                                COMPLETED
                    </Text>
                        </View>
                    </View>
                    <View
                        style={garis}
                    />
                </Content>
            </Container>
        );
    }
}

