import React from 'react';
import {
    Container,
    Button,
    Left,
    Header,
    Title,
    Body,
    Icon,
    Content,
    Right,
    Text,
    View,
} from 'native-base';
import { styles } from './BookingHistory';

export default class TiketKeluhan extends React.Component {
    render() {

        const { flex1, flex2, flex3, date3, email, tombol, headerIcon, bodyHeader, font, fonotif, temail, garis3 } = styles;

        return (
            <Container>
                <Header>
                    <Left style={flex1} />
                    <Body style={bodyHeader}>
                        <Title style={font}>Tiket Keluhan</Title>
                    </Body>
                    <Right style={flex1}>
                        <Button transparent style={tombol}>
                            <Icon style={headerIcon} name="ios-refresh" />
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'space-around', marginTop: -10 }}>
                        <Left style={flex1}><Text style={{ marginTop: 25 }}><Icon style={email} name='mail' /></Text></Left>
                        <Body style={flex3}><Text style={fonotif}>Aplikasi Error</Text></Body>
                        <Right style={flex2}><Text style={date3}>14 Jan 18</Text></Right>
                    </View>
                    <View style={{ padding: 20, marginTop: -45, marginLeft: 20 }}>
                        <Body><Text note>terjadi error saat saya akan memesan</Text></Body>
                    </View>
                    <View style={garis3} />
                    <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'space-around', marginTop: -10 }}>
                        <Left style={flex1}><Text style={{ marginTop: 25 }}><Icon style={email} name='mail' /></Text></Left>
                        <Body style={flex3}><Text style={fonotif}>Pembayaran Gagal</Text></Body>
                        <Right style={flex2}><Text style={date3}>14 Jan 18</Text></Right>
                    </View>
                    <View style={{ padding: 20, marginTop: -45, marginLeft: 20 }}>
                        <Body><Text note>terjadi error saat saya akan memesan</Text></Body>
                    </View>
                    <View style={garis3} />
                    <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'space-around', marginTop: -10 }}>
                        <Left style={flex1}><Text style={{ marginTop: 25 }}><Icon style={email} name='mail' /></Text></Left>
                        <Body style={flex3}><Text style={fonotif}>Gagal Check Out</Text></Body>
                        <Right style={flex2}><Text style={date3}>14 Jan 18</Text></Right>
                    </View>
                    <View style={{ padding: 20, marginTop: -45, marginLeft: 20 }}>
                        <Body><Text note>terjadi error saat saya akan memesan</Text></Body>
                    </View>
                    <View style={garis3} />
                    {/* <View style={{ padding: 20, flexDirection: 'row' }}>
                    <Left style={flex1}>
                    <Text style={temail}><Icon style={email} name="mail" /></Text>
                    <Text style={{ marginTop: 40 }}><Icon style={email} name="mail" /></Text>
                    <Text style={{ marginTop: 40 }}><Icon style={email} name="mail" /></Text>
                    </Left>
                    <Body style={flex3}>
                    <Text style={fonotif}>Aplikasi Error</Text>
                    <Text style={fonotif}>Pembayaran Gagal</Text>
                    <Text style={fonotif}>Gagal Check Out</Text>
                    </Body>
                    <Right style={flex2}>
                    <Text style={date3}>14 Jan 2018</Text>
                    <Text style={{ marginTop: 45 }}>12 Jan 2018</Text>
                    <Text style={{ marginTop: 45 }}>10 Jan 2018</Text>
                    </Right>
                    </View> */}
                </Content>
            </Container>
        )
    };
}