import React, { Component } from 'react';
import {
    Container,
    Header,
    Footer,
    Button,
    Left,
    Item,
    Title,
    Body,
    Icon,
    Form,
    Input,
    List,
    Content,
    Right,
    Text,
    View,
    ListItem,
    Radio
} from 'native-base';
import { styles } from './BookingHistory';

class Keluhan extends Component {
    render() {
        return (
            <Input
                {...this.props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
                editable={true}
            />
        );
    }
}

export default class TiketBantuan extends React.Component {
    render() {
        const { headerIcon, flex1, tombol, font, check, date, date2, price, fasilitas } = styles;
        return (
            <Container>
                <Header>
                    <Left style={flex1} />
                    <Body style={styles.bodyHeader}>
                        <Title style={font}>RIWAYAT PEMESANAN</Title>
                    </Body>
                    <Right style={flex1}>
                        <Button transparent style={tombol}>
                            <Icon style={headerIcon} name="ios-refresh" />
                        </Button>
                    </Right>
                </Header>                
                <Content>
                    <View style={{ alignItems: 'center', paddingVertical: 130, flexDirection: 'column' }}>
                <Text>
                <Icon style={{ fontSize: 130 }} name='archive' />
                </Text>
                <Text style={{ fontSize:18 }}>Belum ada data pemesanan</Text>
                </View>
                </Content>
            </Container>
        );
    }
}