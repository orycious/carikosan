import React from 'react';
import {
    Container,
    Header,
    Footer,
    Button,
    Left,
    Title,
    Body,
    Icon,
    Content,
    Right,
    Text,
    View
} from 'native-base';
import theme from '@native-base-theme/variables/commonColorSecondary';
import { styles } from './BookingHistory';

export default class DetailRiwayat extends React.Component {
    render() {
        const { garis2, flex1, namakos, font, check, date, date2, price, fasilitas  } = styles;
        return (
            <Container>
                <Header>
                    <Left style={flex1} />
                    <Body style={styles.bodyHeader}>
                        <Title style={font}>CK1802000375</Title>
                    </Body>
                    <Right style={flex1} />
                </Header>
                <Content>
                    <View style={{ padding: 20 }}>
                        <Text style={namakos}>Kos Cipendawa [TEST]</Text>
                    </View>
                    <View
                        style={garis2}
                    />
                    <View style={check}>
                    <Text>Check In</Text>                
                    <Text>Check Out</Text>                
                    </View>
                    <View style={check}>
                        <Text style={date}>Senin, 5 Feb 2018</Text>
                    <Body>
                        <Text><Icon style={{ fontSize: 20 }} name="ios-arrow-forward" /></Text>
                    </Body>
                        <Text style={date2}>Selasa, 6 Feb 2018</Text>
                    </View>
                    <View style={{ padding: 20, marginTop: 10 }}>
                    <Text style={{ marginBottom: 10 }}>Tipe kamar                    :   Male / Pria</Text>
                        <Text style={{ marginBottom: 10 }}>Kapasitas kamar         :   2</Text>
                        <Text style={{ marginBottom: 6 }}>Fasilitas                         :  Keamanan Terjaga,</Text>
                        <Text style={fasilitas}>Sumer Air Tanah,</Text>
                        <Text style={fasilitas}>Kulkas / Refrigerator,</Text>
                        <Text style={fasilitas}>Shared Bathroom / Kamar Mandi Luar</Text>
                    </View>
                    <View style={{ padding: 20, flexDirection: 'row' }}>
                    <Left>
                    <Text style={price}>Harga (Bulanan)</Text>
                    <Text style={price}>Jumlah Kamar</Text>
                    <Text style={price}>Biaya Layanan</Text>
                            <View style={garis2} />
                    <Text style={price}>Total Harga</Text>
                            <View style={garis2} />
                    </Left>
                    <Right>
                    <Text style={price}>Rp. 30.000</Text>
                    <Text style={price}>1</Text>
                    <Text style={price}>Rp. 20.000</Text>
                            <View style={garis2} />
                    <Text style={price}>Rp. 50.000</Text>
                            <View style={garis2} />
                    </Right>
                    </View>
                </Content>
                <Footer style={{ backgroundColor: 'white', alignItems: 'center' }}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                        <Button bordered rounded style={{ width: 150, height: 40, marginLeft: 15, justifyContent: 'center', backgroundColor: 'white', borderColor: 'rgb(240, 64, 87)' }}>
                            <Text style={{ color: '#f04057' }}>CHECK  IN</Text>
                        </Button>
                        <Button rounded style={{ width: 150, height: 40, marginRight: 15, justifyContent: 'center', backgroundColor: '#f04057' }}>
                            <Text style={{ color: 'white' }}>CANCEL</Text>
                        </Button>
                    </View>
                </Footer>
            </Container>
        );
    }
}
