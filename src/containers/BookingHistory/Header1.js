import React from 'react';
import { styles } from './BookingHistory';
import {
    Left,
    Right,
    Body,
    Button,
    Title,
    Icon
} from 'native-base';

const { flex1 } = styles;
const Header1 = () => (
    <Header>
        <Left style={flex1} />
        <Body style={bodyHeader}>
            <Title style={styles.font}>Riwayat Pemesanan</Title>
        </Body>
        <Right style={flex1}>
            <Button transparent style={styles.tombol}>
                <Icon style={styles.headerIcon} name="ios-refresh" />
            </Button>
        </Right>
    </Header>
);

export { Header1 };
