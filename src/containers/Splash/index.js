import React from 'react';
import {
  Container,
  Content,
  H3
} from 'native-base';
import {
  Image,
  ImageBackground,
  StyleSheet
} from 'react-native';
import colors from '@lib/colors';

export default class Splash extends React.Component {
  render() {
    return (
      <ImageBackground
        source={require('@images/bg.jpg')}
        style={styles.imageBackground}>
        <Container>
          <Content
            padder
            contentContainerStyle={styles.content}>

            <Image source={require('@images/logo.png')} style={styles.image} />
            <H3 style={styles.heading}>ONE STOP SOLUTION</H3>
            <H3 style={styles.heading}>FOR YOUR SHORT / LONG STAY</H3>

          </Content>
        </Container>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  image: { marginVertical: 20 },
  heading: {
    color: colors.secondary.textLight,
    fontWeight: '100',
    marginVertical: 5,
    fontFamily: 'Roboto'
  },
  content: {
    alignItems: 'center',
    paddingTop: 50
  },
  imageBackground: { flex: 1 },
})