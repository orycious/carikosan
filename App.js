import React, { Component } from 'react';

import Router from './Router';

import { StyleProvider } from 'native-base';
import getTheme from '@native-base-theme/components';
import commonColor from '@native-base-theme/variables/commonColorPrimary';

export default class App extends Component{
  render() {
    return (
      <StyleProvider style={getTheme(commonColor)}>
        <Router />
      </StyleProvider>
    );
  }
}